package estructuras;

import java.util.*;
import java.math.*;

public class Estructuras {

    public static void main(String[] args) {
        matriz(args);
    }
    public static void matriz(String[] args) {
        
        System.out.println("Ingrese el número de Filas: ");
        Scanner var = new Scanner(System.in);
        int a = var.nextInt(); // filas
        System.out.println("Ingrese el número de Columnas: ");
        Scanner var0 = new Scanner(System.in);
        int b = var0.nextInt(); // columnas
        int[][] numeros = new int[a][b]; // a son las filas, y b las columnas
        
        for (int i = 0; i < numeros.length; i++) {
            for (int j = 0; j < numeros[i].length; j ++){
                int c = (int)(Math.random() * 20);
                numeros[i][j] = (c);
            }
        }
        for (int k = 0; k < numeros.length; k++) {
            System.out.println("|");
            for (int l = 0; l < numeros[k].length;l++){
                System.out.print("|");
                System.out.print(numeros[k][l]);
                
            }
        
        }
        System.out.print("|\n");
        int d = 0;
        for (int x = 0; x < numeros.length; x++) {
            if (numeros[x][0] == 5){
                d +=1;
            }
        }
    System.out.print("Cantidad de Columnas que empiezan con 5: "+d+"\n");    
    }
}